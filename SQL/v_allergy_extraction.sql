--This convoluted query is necessary to split the whitespace-separated allergy list in order to count
-- specific allergies separately for each patient.
-- Query: count of encounters per allergy and per patient for all naive patients.
select explorys_patient_id,allergy,x.concept_name, count(1) as nencounters from 
	(select explorys_patient_id, std_allergy as allergies, num as allergy_idx, split_part(std_allergy, ' ', n.num) as allergy from SUPERMART_200_S_02_10_2017.v_allergy 
		cross join (select row_number() over () as num from all_tables limit 10) as n
		where split_part(std_allergy, ' ', n.num) <> ''
	) as t
left join XREF.snomed as x on allergy = x.concept_id
group by explorys_patient_id,allergy,x.concept_name
order by explorys_patient_id,nencounters desc;

-- counts aggregated over patients
select allergy,x.concept_name, count(1) as nencounters from 
	(select explorys_patient_id, std_allergy as allergies, num as allergy_idx, split_part(std_allergy, ' ', n.num) as allergy from SUPERMART_200_S_02_10_2017.v_allergy 
		cross join (select row_number() over () as num from all_tables limit 10) as n
		where split_part(std_allergy, ' ', n.num) <> ''
	) as t
left join XREF.snomed as x on allergy = x.concept_id
group by allergy,x.concept_name
order by nencounters desc;

SELECT explorys_patient_id,std_allergy,encounter FROM "SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_patient_allergy_enc";

SELECT count(distinct std_allergy) FROM SUPERMART_200_S_02_10_2017.v_allergy as a
inner join SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.AllNaivePatients as p on a.explorys_patient_id = p.x;

SELECT distinct std_allergy FROM SUPERMART_200_S_02_10_2017.v_allergy as a
inner join SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.AllNaivePatients as p on a.explorys_patient_id = p.x;

select explorys_patient_id, allergies,num,allergy,x.concept_name from 
	(select explorys_patient_id, std_allergy as allergies, num, split_part(std_allergy, ' ', n.num) as allergy from SUPERMART_200_S_02_10_2017.v_allergy as a
		cross join(select row_number() over () as num from all_tables limit 10) as n
		inner join SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.AllNaivePatients as p on a.explorys_patient_id = p.x;
		where (length(std_allergy)>(9+1)*1) and split_part(std_allergy, ' ', n.num) <> ''
	) as t
left join XREF.snomed as x on allergy = x.concept_id order by explorys_patient_id, num;

select std_allergy, x.concept_name  from SUPERMART_200_S_02_10_2017.v_allergy as a
left join XREF.snomed as x on a.std_allergy = x.concept_id group by std_allergen_type, concept_name;