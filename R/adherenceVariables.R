getAdherenceVariables <- function(aggregateMutateMaster, masterTable){
  
  
  
  prescriptionLags <- masterTable %>%
    filter(!is.na(prescription_date)) %>%
    arrange(encounter_date) %>%
    mutate(
      prescriptionLag = as.numeric(as.Date(encounter_date) - lag(as.Date(encounter_date)))
    ) %>%
    group_by(explorys_patient_id, lineTreatment) %>%
    summarise(
      meanPrescLag = mean(prescriptionLag, na.rm = T)
  )
  
  aggregateMutateMaster %>%
    left_join(prescriptionLags) %>%
    mutate(
      meanPrescLag = ifelse(totalPrescriptions < 2, daysOnCurrentTreatment, meanPrescLag),
      totalPrescriptionsPerYear = totalPrescriptions / (daysOnCurrentTreatment / 365),
      adh1_hasMultiplePrescriptions = totalPrescriptions > 1,
      adh2_hasRegularPrescriptions = totalPrescriptionsPerYear >= ( ( 0.7*daysOnCurrentTreatment ) / meanPrescLag),
      adh2_hasRegularPrescriptions = ifelse(is.na(adh2_hasRegularPrescriptions), F, adh2_hasRegularPrescriptions),
      adh3_hasMultipleRefills = totalRefills >= 1,
      adh4_refillsVsTime = ifelse(currentDrug == 'alemtuzumab', ceiling(daysOnCurrentTreatment / 365) == distinctYearsWithPrescription, 
                                  ifelse(currentDrug == 'nataluzumab', totalPrescriptionsPerYear >= 12,
                                         (totalRefills / monthsOnDrug) >= 0.7)),
      adh4_refillsVsTime = ifelse(is.na(adh4_refillsVsTime), F, adh4_refillsVsTime),
      adh_combined = adh4_refillsVsTime | (adh1_hasMultiplePrescriptions & adh2_hasRegularPrescriptions) 

    )
  
  
}