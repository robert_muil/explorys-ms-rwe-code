getRelapses <- function(){
  
  methylDosageDrug <- dbGetQuery(
    env$sm, "SELECT
    *
    FROM supermart_200.v_drug
    WHERE 
    all_ndc_codes LIKE '%54569-1036%' OR ndc_code LIKE '%54569-1036%' OR display_ndc LIKE '%54569-1036%' OR
    all_ndc_codes LIKE '%33261-0335%' OR ndc_code LIKE '%33261-0335%' OR display_ndc LIKE '%33261-0335%' OR
    all_ndc_codes LIKE '%63874-0413%' OR ndc_code LIKE '%63874-0413%' OR display_ndc LIKE '%63874-0413%' OR
    all_ndc_codes LIKE '%68084-0149%' OR ndc_code LIKE '%68084-0149%' OR display_ndc LIKE '%68084-0149%' OR
    all_ndc_codes LIKE '%00555-0301%' OR ndc_code LIKE '%00555-0301%' OR display_ndc LIKE '%00555-0301%' OR
    all_ndc_codes LIKE '%68001-0005%' OR ndc_code LIKE '%68001-0005%' OR display_ndc LIKE '%68001-0005%' OR
    all_ndc_codes LIKE '%51991-0188%' OR ndc_code LIKE '%51991-0188%' OR display_ndc LIKE '%51991-0188%' OR
    all_ndc_codes LIKE '%63629-3910%' OR ndc_code LIKE '%63629-3910%' OR display_ndc LIKE '%63629-3910%' OR
    all_ndc_codes LIKE '%58118-0001%' OR ndc_code LIKE '%58118-0001%' OR display_ndc LIKE '%58118-0001%' OR
    all_ndc_codes LIKE '%61919-0149%' OR ndc_code LIKE '%61919-0149%' OR display_ndc LIKE '%61919-0149%' OR
    all_ndc_codes LIKE '%55045-1259%' OR ndc_code LIKE '%55045-1259%' OR display_ndc LIKE '%55045-1259%' OR
    all_ndc_codes LIKE '%59762-3327%' OR ndc_code LIKE '%59762-3327%' OR display_ndc LIKE '%59762-3327%' OR
    all_ndc_codes LIKE '%59762-4440%' OR ndc_code LIKE '%59762-4440%' OR display_ndc LIKE '%59762-4440%' OR
    all_ndc_codes LIKE '%59762-0049%' OR ndc_code LIKE '%59762-0049%' OR display_ndc LIKE '%59762-0049%' OR
    all_ndc_codes LIKE '%59762-0050%' OR ndc_code LIKE '%59762-0050%' OR display_ndc LIKE '%59762-0050%' OR
    all_ndc_codes LIKE '%59762-0051%' OR ndc_code LIKE '%59762-0051%' OR display_ndc LIKE '%59762-0051%' OR
    all_ndc_codes LIKE '%59746-0001%' OR ndc_code LIKE '%59746-0001%' OR display_ndc LIKE '%59746-0001%' OR
    all_ndc_codes LIKE '%59746-0002%' OR ndc_code LIKE '%59746-0002%' OR display_ndc LIKE '%59746-0002%' OR
    all_ndc_codes LIKE '%59746-0003%' OR ndc_code LIKE '%59746-0003%' OR display_ndc LIKE '%59746-0003%' OR
    all_ndc_codes LIKE '%59746-0015%' OR ndc_code LIKE '%59746-0015%' OR display_ndc LIKE '%59746-0015%' OR
    all_ndc_codes LIKE '%00179-0124%' OR ndc_code LIKE '%00179-0124%' OR display_ndc LIKE '%00179-0124%' OR
    all_ndc_codes LIKE '%68387-0170%' OR ndc_code LIKE '%68387-0170%' OR display_ndc LIKE '%68387-01704%' OR
    all_ndc_codes LIKE '%49999-0153%' OR ndc_code LIKE '%49999-0153%' OR display_ndc LIKE '%49999-0153%' OR
    all_ndc_codes LIKE '%35356-0763%' OR ndc_code LIKE '%35356-0763%' OR display_ndc LIKE '%35356-0763%' OR
    all_ndc_codes LIKE '%54868-2913%' OR ndc_code LIKE '%54868-2913%' OR display_ndc LIKE '%54868-2913%' OR
    all_ndc_codes LIKE '%54868-6624%' OR ndc_code LIKE '%54868-6624%' OR display_ndc LIKE '%54868-6624%' OR
    all_ndc_codes LIKE '%68788-4593%' OR ndc_code LIKE '%68788-4593%' OR display_ndc LIKE '%68788-4593%' OR
    all_ndc_codes LIKE '%68788-9218%' OR ndc_code LIKE '%68788-9218%' OR display_ndc LIKE '%68788-9218%' OR
    all_ndc_codes LIKE '%63187-0160%' OR ndc_code LIKE '%63187-0160%' OR display_ndc LIKE '%63187-0160%' OR
    all_ndc_codes LIKE '%00603-4593%' OR ndc_code LIKE '%00603-4593%' OR display_ndc LIKE '%00603-4593%' OR
    all_ndc_codes LIKE '%21695-0080%' OR ndc_code LIKE '%21695-0080%' OR display_ndc LIKE '%21695-0080%' OR
    all_ndc_codes LIKE '%49349-0884%' OR ndc_code LIKE '%49349-0884%' OR display_ndc LIKE '%49349-0884%' OR
    all_ndc_codes LIKE '%49349-0969%' OR ndc_code LIKE '%49349-0969%' OR display_ndc LIKE '%49349-0969%' OR
    all_ndc_codes LIKE '%49349-0750%' OR ndc_code LIKE '%49349-0750%' OR display_ndc LIKE '%49349-0750%' OR
    all_ndc_codes LIKE '%00781-5022%' OR ndc_code LIKE '%00781-5022%' OR display_ndc LIKE '%00781-5022%' OR
    all_ndc_codes LIKE '%42549-0522%' OR ndc_code LIKE '%42549-0522%' OR display_ndc LIKE '%42549-0522%' OR
    all_ndc_codes LIKE '%16590-0149%' OR ndc_code LIKE '%16590-0149%' OR display_ndc LIKE '%16590-0149%' OR
    all_ndc_codes LIKE '%50436-4037%' OR ndc_code LIKE '%50436-4037%' OR display_ndc LIKE '%50436-4037%' 
    "
  )
  
  msDiagnoses <- dbGetQuery(
    env$sm, "SELECT 
    explorys_patient_id, icd_code, encounter_join_id, diagnosis_date, is_principal, is_admitting, is_reason_for_visit 
    FROM 
    supermart_200.v_diagnosis 
    WHERE icd_code LIKE '%340%' OR icd_code LIKE '%G35%'"
  )
  
  drugDosageTable  <-as.data.table(dbGetQuery(env$sm,"select * from supermart_200.v_drug where prescription_date<='2016-12-16' and (all_ndc_codes like '%58468-0200%' 
or all_ndc_codes like '%00078-0569%' or all_ndc_codes like '%00078-0607%' or all_ndc_codes like '%58468-0210%' or all_ndc_codes like '%58468-0211%'
or all_ndc_codes like '%59075-0730%' or all_ndc_codes like '%64406-0008%' or all_ndc_codes like '%68546-0317%' or all_ndc_codes like '%68546-0325%' or all_ndc_codes like '%64406-0005%'
or all_ndc_codes like '%64406-0006%' or all_ndc_codes like '%64406-0007%' or all_ndc_codes like '%64406-0011%' or all_ndc_codes like '%64406-0012%' or all_ndc_codes like '%64406-0015%'
or all_ndc_codes like '%64406-0016%' or all_ndc_codes like '%44087-0022%' or all_ndc_codes like '%44087-0044%' or all_ndc_codes like '%44087-8822%' or all_ndc_codes like '%44087-0188%'
or all_ndc_codes like '%44087-3322%' or all_ndc_codes like '%44087-3344%' or all_ndc_codes like '%59627-0001%' or all_ndc_codes like '%59627-0002%' or all_ndc_codes like '%59627-0003%'
or all_ndc_codes like '%59627-0111%' or all_ndc_codes like '%59627-0222%' or all_ndc_codes like '%59627-0333%' or all_ndc_codes like '%00781-3234%' or display_ndc like '%58468-0200%'
or all_ndc_codes like '%50242-0051%' or all_ndc_codes like '%50242-0053%' or all_ndc_codes like '%00074-0033%' or all_ndc_codes like '%00004-0501%'
or display_ndc like '%00078-0569%' or display_ndc like '%00078-0607%' or display_ndc like '%58468-0210%' or display_ndc like '%58468-0211%'
or display_ndc like '%5-075-0730%' or display_ndc like '%64406-0008%' or display_ndc like '%68546-0317%' or display_ndc like '%68546-0325%' or display_ndc like '%64406-0005%'
or display_ndc like '%64406-0006%' or display_ndc like '%64406-0007%' or display_ndc like '%64406-0011%' or display_ndc like '%64406-0012%' or display_ndc like '%64406-0015%'
or display_ndc like '%64406-0016%' or display_ndc like '%44087-0022%' or display_ndc like '%44087-0044%' or display_ndc like '%44087-8822%' or display_ndc like '%44087-0188%'
or display_ndc like '%44087-3322%' or display_ndc like '%44087-3344%' or display_ndc like '%59627-0001%' or display_ndc like '%59627-0002%' or display_ndc like '%59627-0003%'
or display_ndc like '%59627-0111%' or display_ndc like '%59627-0222%' or display_ndc like '%59627-0333%' or display_ndc like '%00781-3234%' or display_ndc like '%50242-0051%' 
or display_ndc like '%50242-0053%' or display_ndc like '%00074-0033%' or display_ndc like '%00004-0501%' 
or ndc_code like '%58468-0200%' or ndc_code like '%00078-0569%' or ndc_code like '%00078-0607%' or ndc_code like '%58468-0210%' or ndc_code like '%58468-0211%'                
or ndc_code like '%59075-0730%' or ndc_code like '%64406-0008%' or ndc_code like '%68546-0317%' or ndc_code like '%68546-0325%' or ndc_code like '%64406-0005%'
or ndc_code like '%64406-0006%' or ndc_code like '%64406-0007%' or ndc_code like '%64406-0011%' or ndc_code like '%64406-0012%' or ndc_code like '%64406-0015%'
or ndc_code like '%64406-0016%' or ndc_code like '%44087-0022%' or ndc_code like '%44087-0044%' or ndc_code like '%44087-8822%' or ndc_code like '%44087-0188%'
or ndc_code like '%44087-3322%' or ndc_code like '%44087-3344%' or ndc_code like '%59627-0001%' or ndc_code like '%59627-0002%' or ndc_code like '%59627-0003%'
or ndc_code like '%59627-0111%' or ndc_code like '%59627-0222%' or ndc_code like '%59627-0333%' or ndc_code like '%00781-3234%'
or ndc_code like '%50242-0051%' or ndc_code like '%50242-0053%' or ndc_code like '%00074-0033%' or ndc_code like '%00004-0501%' 
or rx_cui like '1310526' or rx_cui like '153326' or rx_cui like '135779' or rx_cui like '860241' or rx_cui like '1012896' or
rx_cui like '1594659' or rx_cui like '1546173' or rx_cui like '228271' or rx_cui like '1373484' or rx_cui like '492635' or
rx_cui like '1656631' or rx_cui like '828267' or rx_cui like '284679' or rx_cui like '1172298' or rx_cui like '1594659' or 
rx_cui like '1594660' or rx_cui like '1594662' or rx_cui like '1594663' or rx_cui like '1594657' or rx_cui like '1594658' or
rx_cui like '117055' or rx_cui like '1164265' or rx_cui like '828265' or rx_cui like '1656628' or rx_cui like '1310526' or
rx_cui like '1310527' or rx_cui like '1310529' or rx_cui like '1310530' or rx_cui like '1310531'  or rx_cui like '1310534' or
rx_cui like '1310535' or rx_cui like '1310520' or rx_cui like '1310521'  or rx_cui like '1310532'  or rx_cui like '1310525' or
rx_cui like '1310525'  or rx_cui like '1310522'  or rx_cui like '1310523' or rx_cui like '1310533' or rx_cui like '1795335' or
rx_cui like '190353') "))
  
  # removing NAs from drugTable
  drugTablenoNA<-drugTable[complete.cases(drugTable)]
  drugTablenoNA$prescription_date <-as.Date(drugTablenoNA$prescription_date)
  
  # Get dosage frequencies
  doseFreq <- drugDosageTable %>% dplyr::group_by(brand_name_descriptions,std_dosage) %>%
    dplyr::summarise(freq = n())
  
  descFreq <- drugDosageTable %>% dplyr::group_by(brand_name_descriptions,concept_description,std_dosage) %>%
    dplyr::summarise(freq = n())
  
  molFreq <- drugDosageTable %>% dplyr::group_by(brand_name_descriptions,ingredient_descriptions) %>%
    dplyr::summarise(freq = n())
  
  write.csv(descFreq, file = "DoseFreq.csv")
  
}