# 
# 
# 
# 
# 
# 
# 
# getRelapses <- function( loadFromDB = T ){
#   
#   if ( !loadFromDB ) {
#     
#     load(paste0(env$saveDir, 'relapseTable.Rdata'))
#     
#   } else {
#     
#     hospitalizations <- dbGetQuery(
#       env$sm, "SELECT 
#       explorys_patient_id, encounter_join_id, std_encounter_type, appt_made_date, encounter_date FROM supermart_200.v_encounter 
#       WHERE std_encounter_type IN ('HOSPITAL_EMERGENCY_ROOM_VISIT', 'URGENT', 'HOSPITAL_ENCOUNTER', 'HOSPITAL_INPATIENT')"
#       )
#     #
#     
#     cortisoneRxCui <- read.csv('Y:\\mckinsey_global/Explorys Upload/rxcui_corticosteroids.csv')
#     cortisoneNDC <- read.csv('Y:\\mckinsey_global/Explorys Upload/ndc_codes_corticosteroids.csv')
#     jcodesDexa <- read.csv('Y:\\mckinsey_global/Explorys Upload/jcodes_dexamethasone.csv', stringsAsFactors = F)
#     jcodesCort <- read.csv('Y:\\mckinsey_global/Explorys Upload/jcodes_corticotropin.csv', stringsAsFactors = F)
#     jcodesMeth <- read.csv('Y:\\mckinsey_global/Explorys Upload/jcodes_methylprednisolone.csv', stringsAsFactors = F)
#     jcodesPsol <- read.csv('Y:\\mckinsey_global/Explorys Upload/jcodes_prednisolone.csv', stringsAsFactors = F)
#     jcodesPson <- read.csv('Y:\\mckinsey_global/Explorys Upload/jcodes_prednisone.csv', stringsAsFactors = F)
#   
#  
#     #drugQuerySuffix_all_ndc_codes = paste(paste0("REPLACE(all_ndc_codes, '-', '') LIKE '%", cortisoneNDC$x, "%'" ), collapse = ' OR ')
#     #drugQuerySuffix_ndc_code = paste(paste0("REPLACE(ndc_code, '-', '') LIKE '%", cortisoneNDC$x, "%'" ), collapse = ' OR ')
#     #drugQuerySuffix_display_ndc = paste(paste0("REPLACE(display_ndc, '-', '') LIKE '%", cortisoneNDC$x, "%'" ), collapse = ' OR ')
#     drugQuerySuffix_rx_cui = paste(paste0("rx_cui LIKE '%", cortisoneRxCui$rxcui, "%'" ), collapse = ' OR ')
#     procQuerySuffix_cpt_concept = paste(paste0("cpt_concept LIKE '%", c(jcodesDexa$x, jcodesCort$x, jcodesMeth$x, jcodesPson$x, jcodesPsol$x), "%'" ), collapse = ' OR ')
#     
#     drugQuery = paste( "SELECT encounter_join_id, explorys_patient_id, all_ndc_codes, brand_name_descriptions, display_ndc, ndc_code, std_drug_desc FROM supermart_200.v_drug WHERE", drugQuerySuffix_rx_cui)
#     procQuery = paste( "SELECT encounter_join_id, explorys_patient_id, cpt_concept,proc_date FROM supermart_200.v_procedure WHERE", procQuerySuffix_cpt_concept)
#     
#     
#     methylDrug <- dbGetQuery(env$sm, drugQuery)
#     methylProc <- dbGetQuery(env$sm, procQuery)
#   
#   # methylDrug <- dbGetQuery(
#   #   env$sm, "SELECT
#   #   encounter_join_id, explorys_patient_id, all_ndc_codes, brand_name_descriptions, display_ndc, ndc_code, std_drug_desc
#   #   FROM supermart_200.v_drug
#   #   WHERE 
#   #   REPLACE(all_ndc_codes, '-', '') LIKE '%545691036%' OR ndc_code LIKE '%545691036%' OR display_ndc LIKE '%545691036%'")
#   # 
#   # 
#   # #get treatments with methylprednisolone from v_drug  
#   #   methylDrug <- dbGetQuery(
#   #     env$sm, "SELECT
#   #     encounter_join_id, explorys_patient_id, all_ndc_codes, brand_name_descriptions, display_ndc, ndc_code, std_drug_desc
#   #     FROM supermart_200.v_drug
#   #     WHERE 
#   #     all_ndc_codes LIKE '%54569-1036%' OR ndc_code LIKE '%54569-1036%' OR display_ndc LIKE '%54569-1036%' OR
#   #     all_ndc_codes LIKE '%33261-0335%' OR ndc_code LIKE '%33261-0335%' OR display_ndc LIKE '%33261-0335%' OR
#   #     all_ndc_codes LIKE '%63874-0413%' OR ndc_code LIKE '%63874-0413%' OR display_ndc LIKE '%63874-0413%' OR
#   #     all_ndc_codes LIKE '%68084-0149%' OR ndc_code LIKE '%68084-0149%' OR display_ndc LIKE '%68084-0149%' OR
#   #     all_ndc_codes LIKE '%00555-0301%' OR ndc_code LIKE '%00555-0301%' OR display_ndc LIKE '%00555-0301%' OR
#   #     all_ndc_codes LIKE '%68001-0005%' OR ndc_code LIKE '%68001-0005%' OR display_ndc LIKE '%68001-0005%' OR
#   #     all_ndc_codes LIKE '%51991-0188%' OR ndc_code LIKE '%51991-0188%' OR display_ndc LIKE '%51991-0188%' OR
#   #     all_ndc_codes LIKE '%63629-3910%' OR ndc_code LIKE '%63629-3910%' OR display_ndc LIKE '%63629-3910%' OR
#   #     all_ndc_codes LIKE '%58118-0001%' OR ndc_code LIKE '%58118-0001%' OR display_ndc LIKE '%58118-0001%' OR
#   #     all_ndc_codes LIKE '%61919-0149%' OR ndc_code LIKE '%61919-0149%' OR display_ndc LIKE '%61919-0149%' OR
#   #     all_ndc_codes LIKE '%55045-1259%' OR ndc_code LIKE '%55045-1259%' OR display_ndc LIKE '%55045-1259%' OR
#   #     all_ndc_codes LIKE '%59762-3327%' OR ndc_code LIKE '%59762-3327%' OR display_ndc LIKE '%59762-3327%' OR
#   #     all_ndc_codes LIKE '%59762-4440%' OR ndc_code LIKE '%59762-4440%' OR display_ndc LIKE '%59762-4440%' OR
#   #     all_ndc_codes LIKE '%59762-0049%' OR ndc_code LIKE '%59762-0049%' OR display_ndc LIKE '%59762-0049%' OR
#   #     all_ndc_codes LIKE '%59762-0050%' OR ndc_code LIKE '%59762-0050%' OR display_ndc LIKE '%59762-0050%' OR
#   #     all_ndc_codes LIKE '%59762-0051%' OR ndc_code LIKE '%59762-0051%' OR display_ndc LIKE '%59762-0051%' OR
#   #     all_ndc_codes LIKE '%59746-0001%' OR ndc_code LIKE '%59746-0001%' OR display_ndc LIKE '%59746-0001%' OR
#   #     all_ndc_codes LIKE '%59746-0002%' OR ndc_code LIKE '%59746-0002%' OR display_ndc LIKE '%59746-0002%' OR
#   #     all_ndc_codes LIKE '%59746-0003%' OR ndc_code LIKE '%59746-0003%' OR display_ndc LIKE '%59746-0003%' OR
#   #     all_ndc_codes LIKE '%59746-0015%' OR ndc_code LIKE '%59746-0015%' OR display_ndc LIKE '%59746-0015%' OR
#   # all_ndc_codes LIKE '%00179-0124%' OR ndc_code LIKE '%00179-0124%' OR display_ndc LIKE '%00179-0124%' OR
#   # all_ndc_codes LIKE '%68387-0170%' OR ndc_code LIKE '%68387-0170%' OR display_ndc LIKE '%68387-01704%' OR
#   # all_ndc_codes LIKE '%49999-0153%' OR ndc_code LIKE '%49999-0153%' OR display_ndc LIKE '%49999-0153%' OR
#   # all_ndc_codes LIKE '%35356-0763%' OR ndc_code LIKE '%35356-0763%' OR display_ndc LIKE '%35356-0763%' OR
#   # all_ndc_codes LIKE '%54868-2913%' OR ndc_code LIKE '%54868-2913%' OR display_ndc LIKE '%54868-2913%' OR
#   # all_ndc_codes LIKE '%54868-6624%' OR ndc_code LIKE '%54868-6624%' OR display_ndc LIKE '%54868-6624%' OR
#   # all_ndc_codes LIKE '%68788-4593%' OR ndc_code LIKE '%68788-4593%' OR display_ndc LIKE '%68788-4593%' OR
#   # all_ndc_codes LIKE '%68788-9218%' OR ndc_code LIKE '%68788-9218%' OR display_ndc LIKE '%68788-9218%' OR
#   # all_ndc_codes LIKE '%63187-0160%' OR ndc_code LIKE '%63187-0160%' OR display_ndc LIKE '%63187-0160%' OR
#   # all_ndc_codes LIKE '%00603-4593%' OR ndc_code LIKE '%00603-4593%' OR display_ndc LIKE '%00603-4593%' OR
#   # all_ndc_codes LIKE '%21695-0080%' OR ndc_code LIKE '%21695-0080%' OR display_ndc LIKE '%21695-0080%' OR
#   # all_ndc_codes LIKE '%49349-0884%' OR ndc_code LIKE '%49349-0884%' OR display_ndc LIKE '%49349-0884%' OR
#   # all_ndc_codes LIKE '%49349-0969%' OR ndc_code LIKE '%49349-0969%' OR display_ndc LIKE '%49349-0969%' OR
#   # all_ndc_codes LIKE '%49349-0750%' OR ndc_code LIKE '%49349-0750%' OR display_ndc LIKE '%49349-0750%' OR
#   # all_ndc_codes LIKE '%00781-5022%' OR ndc_code LIKE '%00781-5022%' OR display_ndc LIKE '%00781-5022%' OR
#   # all_ndc_codes LIKE '%42549-0522%' OR ndc_code LIKE '%42549-0522%' OR display_ndc LIKE '%42549-0522%' OR
#   # all_ndc_codes LIKE '%16590-0149%' OR ndc_code LIKE '%16590-0149%' OR display_ndc LIKE '%16590-0149%' OR
#   # all_ndc_codes LIKE '%50436-4037%' OR ndc_code LIKE '%50436-4037%' OR display_ndc LIKE '%50436-4037%'")
#   #   
#     #get treatments with methylprednisolone with v_procedure
#     # methylProc <- dbGetQuery(env$sm, "SELECT
#     #                          encounter_join_id, explorys_patient_id, cpt_concept,proc_date FROM supermart_200.v_procedure WHERE cpt_concept = 'J2930'")
#   
#     msDiagnoses <- dbGetQuery(
#       env$sm, "SELECT 
#          explorys_patient_id, icd_code, encounter_join_id, diagnosis_date, is_principal, is_admitting, is_reason_for_visit 
#            FROM 
#            supermart_200.v_diagnosis 
#            WHERE (icd_code LIKE '%340%' OR icd_code LIKE '%G35%')"
#       )
#     
#     
#     relapseTable <- msDiagnoses %>% 
#       left_join(hospitalizations) %>% 
#       left_join(methylDrug) %>%
#       left_join(methylProc) %>%
#       filter(!is.na(std_encounter_type) | !is.na(std_drug_desc) | !is.na(cpt_concept)) %>%
#       mutate(
#         diagnosisMonth  = as.Date(paste0(format(as.Date(diagnosis_date), "%Y-%m"), '-01')),
#         hospitalization = !is.na(std_encounter_type),
#         mpdDrug         = !is.na(std_drug_desc) | !is.na(cpt_concept)
#       )  %>%
#       distinct(explorys_patient_id, diagnosisMonth)
#     
#     # %>% select(explorys_patient_id, diagnosis_date, diagnosisMonth, hospitalization, mpdDrug)
#     
#     save(relapseTable, file = paste0(env$saveDir, 'relapseTable.Rdata'))
#   }
#   
#   
#   
#   return(relapseTable)
#   
#   
#   
#   
#   
#   
#   
# }
# 
# 
# getRelapsesPerDrug <- function(
#   relapseTable,
#   drugTable,
#   initialDate = '2010-01-01', 
#   drugsToExclude = c(),
#   curvColours = c(),
#   xlim = c(0,84), 
#   survFileName = 'relapses.pdf',
#   survExcelName = 'relapses.xlsx', 
#   strataCol = 'effectiveDrug',
#   demographicsTable = c()){
#   
#   
#   
#   ### get the month in which drug was initially prescribed
#   ### for all patients that received a prescription after the initial date
#   drugPerMonth <- drugTable %>% filter(firstPrescDate >= initialDate) %>%
#     mutate(
#       prescriptionMonth = as.Date(paste0(format(as.Date(firstPrescDate), "%Y-%m"),'-01'))
#     ) %>%
#     distinct() %>%
#     group_by(molecule_name, explorys_patient_id, lineTreatment) %>%
#     dplyr::summarise(
#       firstPrescDate = min(firstPrescDate),
#       lastPrescDate = max(lastPrescDate),
#       prescriptionMonth = min(prescriptionMonth)
#     ) 
#   
#   #get the last date each patient is appearing in the system
#   lastAppearancePerPatient = drugPerMonth %>%
#     group_by(explorys_patient_id) %>%
#     summarise(
#       lastAppearance = max(lastPrescDate)
#     )
#   
#   ### expand grid all patients in all months
#   all_months = expand.grid(unique(drugPerMonth$explorys_patient_id),seq(min(drugPerMonth$prescriptionMonth), max(drugPerMonth$prescriptionMonth), by = 'month'))
#   names(all_months) = c('explorys_patient_id', 'prescriptionMonth')
#   
#   drugPerMonthExpand <- drugPerMonth %>% 
#     full_join(all_months) %>%
#     group_by(explorys_patient_id) %>%
#     arrange(prescriptionMonth) %>%
#     mutate(
#       molecule_name = zoo::na.locf(molecule_name, na.rm = F),
#       firstPrescDate = zoo::na.locf(firstPrescDate, na.rm = F),
#       lastPrescDate = zoo::na.locf(lastPrescDate, na.rm = F),
#       lineTreatment = zoo::na.locf(lineTreatment, na.rm = F)
#     ) %>%
#     filter(!is.na(molecule_name)) %>%
#     left_join(lastAppearancePerPatient) %>%
#     filter(elapsed_months(lastAppearance, prescriptionMonth) >= -12)
#     
#   
#   ### get month of relapse
#   relapseTableDates <- relapseTable %>% 
#     dplyr::rename(relapseMonth = diagnosisMonth) %>%
#     mutate(
#       relapse = T
#     ) %>%
#     mutate(
#       relapseMonth = as.Date(paste0(relapseMonth, '-01'))
#     )
#   
#   drugVsRelapse <- drugPerMonthExpand  %>%
#     left_join(relapseTableDates, by = c('explorys_patient_id' = 'explorys_patient_id', 'prescriptionMonth'='relapseMonth')) %>%
#     group_by(explorys_patient_id) %>%
#     mutate(
#       ### remove relapses that occur within the first week of the prescription (rule 3)
#       relapse = ifelse((as.numeric(as.Date(diagnosis_date, format = '%Y-%m-%d') - as.Date(firstPrescDate)) <= 7) & (as.numeric(as.Date(diagnosis_date, format = '%Y-%m-%d') - as.Date(firstPrescDate)) >=0), F, relapse),
#       relapse = ifelse(is.na(relapse), F, relapse),
#       previousDrug = lag(molecule_name),
#       previousLineTreatment = lag(lineTreatment),
#       ### effective Drug is the drug in action -
#       effectiveDrug = ifelse((as.numeric(as.Date(diagnosis_date, format = '%Y-%m-%d') - as.Date(firstPrescDate)) < 0) & (!is.na(diagnosis_date)) & !is.na(previousDrug), previousDrug, molecule_name),
#       effectiveLineTreatment = ifelse((as.numeric(as.Date(diagnosis_date, format = '%Y-%m-%d') - as.Date(firstPrescDate)) < 0) & (!is.na(diagnosis_date)) & !is.na(previousLineTreatment), previousLineTreatment, lineTreatment),
#       ### set to FALSE any relapses before prescription of any drug
#       relapse = ifelse(is.na(effectiveDrug) & relapse, F, relapse),
#       switch = molecule_name != previousDrug
#       #firstMonth = prescriptionMonth == min(prescriptionMonth),
#       #relapseAfterTreatment = relapse & !firstMonth
#     ) 
#   
#   #annual relapse ratio
#   annualRelapseRatioPerPatient <- drugVsRelapse %>%
#     group_by(explorys_patient_id, effectiveDrug) %>%
#     dplyr::summarise(
#       arr = ifelse(
#         elapsed_months(max(prescriptionMonth), min(prescriptionMonth)) !=0, (length(which(relapse)) * 12) / elapsed_months(max(prescriptionMonth), min(prescriptionMonth)),
#         0)
#     )
#   
#   annualRelapseRatioPerDrug <- annualRelapseRatioPerPatient %>%
#     group_by(effectiveDrug) %>%
#     dplyr::summarise(
#       arr = mean(arr)
#     )
#   
#   writeExcel(dataframes = list(annualRelapseRatioPerDrug = annualRelapseRatioPerDrug), writeFile = paste0(env$saveDir, 'arr.xlsx'))
#   
#   
#   #### are switches triggered by a relapse?
#   
#   
#    
#   relapseSurvival <- drugVsRelapse %>%  
#     group_by(explorys_patient_id, effectiveDrug, effectiveLineTreatment) %>%
#     dplyr::summarise(
#       monthsToRelapse = ifelse(
#         any(relapse),
#         elapsed_months(min(prescriptionMonth[which(relapse)]), min(prescriptionMonth)), 
#         elapsed_months(max(prescriptionMonth), min(prescriptionMonth))
#         ),
#       relapse = any(relapse)
#     ) 
#   
#   ####
#   #relapseSurvival$Aubagio = relapseSurvival$effectiveDrug == 'teriflunomide'
#   
#   
#   
#   
#   if (strataCol == 'effectiveDrug'){
#   
#     lty.est = 1 
#     
#     relapseSurvival['strataCol'] = relapseSurvival[,strataCol]
#     
#   } else {
#     
#     ### add demographics table to the 
#     relapseSurvival <- relapseSurvival %>% left_join(demographicsTable)
#     
#     relapseSurvival['strataCol'] = relapseSurvival[,strataCol]
#     
#     lty.est = seq(1, length(unique(relapseSurvival$strataCol[!is.na(relapseSurvival$strataCol)])))
#     
#     writeExcel(list(relapseSurvival = relapseSurvival), writeFile = paste0(env$saveDir, 'relapseSurvival.xlsx'))
#     
#   }
#   
#   # relapseSurvival$firstLine = relapseSurvival$lineTreatment == 1
#   # relapseSurvival$secondLine = relapseSurvival$lineTreatment == 2
#   # 
#   # relapseSurv = survdiff(Surv(time = monthsToRelapse, event = relapse, type = 'right')~factor(ageGroup), data = relapseSurvival[relapseSurvival$effectiveDrug %in% c('alemtuzumab'),])
#   
#   relapseSurvival = relapseSurvival %>%
#     filter(!effectiveDrug %in% drugsToExclude)
#   
#   relapseSurv = survfit(Surv(time = monthsToRelapse, event = relapse, type = 'right')~strata(factor(strataCol)), data = relapseSurvival)
#   
#     if (length(curvColours) == 0){
#       survCol = "gg.def"
#     } else if (length(curvColours) == 1){
#       survCol = curvColours[[1]]
#     } else{
#       survCol = curvColours
#     }
# 
#   
#     survPlot = ggsurv(relapseSurv, plot.cens = F, back.white = T, xlab = 'Time (Months)', ylab = 'Survival without relapse', surv.col = survCol, lty.est = lty.est) + 
#       ggplot2::coord_cartesian(xlim = xlim, ylim = c(0,1)) + 
#       ggplot2::scale_y_continuous(labels = scales::percent) 
#     
#   
#   
#   
#   pdf(paste0(env$outputDir, survFileName), height = 6, width = 10)
#   print(survPlot)
#   dev.off()
#   
#   
#   cols = lapply(c(2:5, 7:10), function(x) summary(relapseSurv)[x])
#   survTable = do.call(data.frame, cols)
#   survTable$strata = sapply(survTable$strata, function(x) {
#     substr(x, regexpr('=', x)+1, nchar(as.character(x)))[[1]]
#     })
#   survTables = lapply(unique(survTable$strata), function(x) survTable[survTable$strata == x,])
#   names(survTables) = unique(survTable$strata)
#   
#   writeExcel(dataframes = survTables, writeFile = paste0(env$saveDir, survExcelName))
#   
# }
# 
# script_get_survival_curve <- function(){
#   
#   relapseTable <- getRelapses(loadFromDB = T)
#   
#   drugCounts <- DrugCounts()
#   # save(drugCounts, file = paste0(env$saveDir, 'drugCounts.Rdata'))
#   load(paste0(env$saveDir, 'drugCounts.Rdata'))
#   drugTable <- drugCounts$lotInfo
#   
#   # snomed codes
#   
#   treatmentDuration <- TreatmentDuration(drugCounts$allDrugsInfo)
#   
#   drugTable <- drugTable %>% left_join(treatmentDuration %>% select(explorys_patient_id, molecule_name, lastPrescDate))
#   
#   
#   relapseTableMpd <- relapseTable %>% filter(mpdDrug)
#   
#   ###### specify patient segments
#   demographics <- dbGetQuery(env$sm, "SELECT explorys_patient_id, std_gender, birth_year  FROM supermart_200.v_demographic")
#   masterTable <- dbGetQuery(env$sm, "SELECT PATIENT_ID, SMOKING_EVER FROM db1.SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.tmp_master_table")
#   
#   
#   
#   demographicsPerPatient <- demographics %>% 
#     group_by(explorys_patient_id) %>%
#     dplyr::summarise(
#       birthYear = max(birth_year, na.rm = T),
#       gender = unique(std_gender)
#     ) %>%
#     mutate(
#       gender = ifelse(gender == '2', 'F', ifelse(gender == '1', 'M', NA))
#     )
#   
#   smokingPerPatient <- masterTable %>%
#     group_by(PATIENT_ID) %>%
#     summarise(
#       smokingEver = any(SMOKING_EVER == 't')
#     )
#   
#   firstPrescPerPatient <- drugTable %>%
#     group_by(explorys_patient_id) %>%
#     summarise(
#       firstPrescDate = min(firstPrescDate, na.rm = T)
#     )
#   
#   demographicsTable <- as.data.frame(firstPrescPerPatient) %>%
#     left_join(demographicsPerPatient) %>%
#     left_join(smokingPerPatient, by = c('explorys_patient_id' = 'PATIENT_ID')) %>%
#     group_by(explorys_patient_id) %>%
#     mutate(
#       ageAtPrescription = year(as.POSIXlt(firstPrescDate)) - birthYear,
#       ageGroup = cut(ageAtPrescription, breaks = c(-0.1, 35, 55, 1000), labels = c('<35', '35-55', '>55'))
#     )
#   
#   
#   getRelapsesPerDrug(
#     relapseTableMpd,
#     drugTable,
#     initialDate = '2010-01-01', 
#     drugsToExclude = c('daclizumab'),
#     curvColours = drugPalette[-which(names(drugPalette) %in% c('daclizumab'))],
#     xlim = c(0,84), 
#     survFileName = 'relapsesMpd2010-2016.pdf',
#     survExcelName = 'relapsesMpd2010-2016.xlsx')
#   
#   
#   
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2010-01-01', 
#     drugsToExclude = c('alemtuzumab', 'daclizumab'),
#     curvColours = drugPalette[-which(names(drugPalette) %in% c('alemtuzumab', 'daclizumab'))],
#     xlim = c(0,84), 
#     survFileName = 'relapses2010-2016.pdf',
#     survExcelName = 'relapses2010-2016.xlsx')
#   
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2015-01-01', 
#     drugsToExclude = c("daclizumab"),
#     curvColours = drugPalette[-which(names(drugPalette) %in% c('daclizumab'))],
#     xlim = c(0,12), 
#     survFileName = 'relapses2015-2016_12months.pdf', 
#     survExcelName = 'relapses2015-2016_12months.xlsx'
#   )
#   
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2010-01-01', 
#     drugsToExclude = c(),
#     curvColours = drugPalette,
#     xlim = c(0,84), 
#     survFileName = 'relapses2010-2016.pdf',
#     survExcelName = 'relapses2010-2016.xlsx')
#   
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2015-01-01', 
#     drugsToExclude = c("daclizumab"),
#     curvColours = drugPalette[-which(names(drugPalette) %in% c('daclizumab'))],
#     xlim = c(0,24), 
#     survFileName = 'relapses2015-2016_24months.pdf', 
#     survExcelName = 'relapses2015-2016_24months.xlsx'
#   )
#   
#   
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable[drugTable$lineTreatment == 2,],
#     initialDate = '2010-01-01', 
#     drugsToExclude = c('alemtuzumab', 'daclizumab'),
#     curvColours = drugPalette[-which(names(drugPalette) %in% c('alemtuzumab', 'daclizumab'))],
#     xlim = c(0,84), 
#     survFileName = 'relapses2010-2016_2ndLoT.pdf', 
#     survExcelName = 'relapses2010-2016_2ndLoT.xlsx'
#   )
#   
#   # Trimmed curve for Aubagio
# 
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2010-01-01', 
#     drugsToExclude = c('alemtuzumab', 'daclizumab'),
#     curvColours = drugPalette[-which(names(drugPalette) %in% c('alemtuzumab', 'daclizumab'))],
#     xlim = c(0,12), 
#     survFileName = 'Aubagio_trim_relapses2015-2016_12months.pdf', 
#     survExcelName = 'Aubagio_trim_relapses2015-2016_12months.xlsx'
#   )
#   
#   # Trimmed curve for Lemtrada
#   
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2015-01-01', 
#     drugsToExclude = c("daclizumab"),
#     curvColours = drugPalette[-which(names(drugPalette) %in% c('daclizumab'))],
#     xlim = c(0,12), 
#     survFileName = 'Lemtrada_trim_relapses2015-2016_12months.pdf', 
#     survExcelName = 'Lemtrada_trim_relapses2015-2016_12months.xlsx'
#   )
#   
#   #Aubagio - gender
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2010-01-01', 
#     drugsToExclude = unique(drugTable$molecule_name[-which(drugTable$molecule_name == 'teriflunomide')]),
#     curvColours = drugPalette[which(names(drugPalette) == 'teriflunomide')],
#     xlim = c(0,84), 
#     survFileName = 'relapses_Aubagio_gender.pdf',
#     survExcelName = 'relapses_Aubagio_gender.xlsx',
#     strataCol = 'gender',
#     demographicsTable = demographicsTable)
#   
#   #Aubagio - age
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2010-01-01', 
#     drugsToExclude = unique(drugTable$molecule_name[-which(drugTable$molecule_name == 'teriflunomide')]),
#     curvColours = drugPalette[which(names(drugPalette) == 'teriflunomide')],
#     xlim = c(0,84), 
#     survFileName = 'relapses_Aubagio_age.pdf',
#     survExcelName = 'relapses_Aubagio_age.xlsx',
#     strataCol = 'ageGroup',
#     demographicsTable = demographicsTable)
#   
#   #Aubagio - smoking
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2010-01-01', 
#     drugsToExclude = unique(drugTable$molecule_name[-which(drugTable$molecule_name == 'teriflunomide')]),
#     curvColours = drugPalette[which(names(drugPalette) == 'teriflunomide')],
#     xlim = c(0,48), 
#     survFileName = 'relapses_Aubagio_smoking.pdf',
#     survExcelName = 'relapses_Aubagio_smoking.xlsx',
#     strataCol = 'smokingEver',
#     demographicsTable = demographicsTable)
#   
#   #Aubagio - line of treatment
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable[drugTable$lineTreatment < 4,],
#     initialDate = '2010-01-01', 
#     drugsToExclude = unique(drugTable$molecule_name[-which(drugTable$molecule_name == 'teriflunomide')]),
#     curvColours = drugPalette[which(names(drugPalette) == 'teriflunomide')],
#     xlim = c(0,84), 
#     survFileName = 'relapses_Aubagio_LoT.pdf',
#     survExcelName = 'relapses_Aubagio_LoT.xlsx',
#     strataCol = 'lineTreatment',
#     demographicsTable = demographicsTable)
#   
#   #Lemtrada - gender
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2015-01-01', 
#     drugsToExclude = unique(drugTable$molecule_name[-which(drugTable$molecule_name == 'alemtuzumab')]),
#     curvColours = drugPalette[which(names(drugPalette) == 'alemtuzumab')],
#     xlim = c(0,24), 
#     survFileName = 'relapses_Lemtrada_gender.pdf',
#     survExcelName = 'relapses_Lemtrada_gender.xlsx',
#     strataCol = 'gender',
#     demographicsTable = demographicsTable)
#   
#   #Lemtrada - age
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2015-01-01', 
#     drugsToExclude = unique(drugTable$molecule_name[-which(drugTable$molecule_name == 'alemtuzumab')]),
#     curvColours = drugPalette[which(names(drugPalette) == 'alemtuzumab')],
#     xlim = c(0,24), 
#     survFileName = 'relapses_Lemtrada_age.pdf',
#     survExcelName = 'relapses_Lemtrada_age.xlsx',
#     strataCol = 'ageGroup',
#     demographicsTable = demographicsTable)
#   
#   #Lemtrada - smoking
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2015-01-01', 
#     drugsToExclude = unique(drugTable$molecule_name[-which(drugTable$molecule_name == 'alemtuzumab')]),
#     curvColours = drugPalette[which(names(drugPalette) == 'alemtuzumab')],
#     xlim = c(0,24), 
#     survFileName = 'relapses_Lemtrada_smoking.pdf',
#     survExcelName = 'relapses_Lemtrada_smoking.xlsx',
#     strataCol = 'smokingEver',
#     demographicsTable = demographicsTable)
#   
#   #Lemtrada - line of treatment
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable[drugTable$lineTreatment < 4,],
#     initialDate = '2015-01-01', 
#     drugsToExclude = unique(drugTable$molecule_name[-which(drugTable$molecule_name == 'alemtuzumab')]),
#     curvColours = drugPalette[which(names(drugPalette) == 'alemtuzumab')],
#     xlim = c(0,24), 
#     survFileName = 'relapses_Lemtrada_LoT.pdf',
#     survExcelName = 'relapses_Lemtrada_LoT.xlsx',
#     strataCol = 'lineTreatment',
#     demographicsTable = demographicsTable)
#   
#   #Lemtrada - smoking
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2015-01-01', 
#     drugsToExclude = unique(drugTable$molecule_name[-which(drugTable$molecule_name == 'alemtuzumab')]),
#     curvColours = drugPalette[which(names(drugPalette) == 'alemtuzumab')],
#     xlim = c(0,84), 
#     survFileName = 'relapses_Lemtrada_smoking.pdf',
#     survExcelName = 'relapses_Lemtrada_smoking.xlsx',
#     strataCol = 'smokingEver',
#     demographicsTable = demographicsTable)
#   
#   #all - line of treatment
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable[drugTable$lineTreatment < 4,],
#     initialDate = '2010-01-01', 
#     drugsToExclude = c(),
#     curvColours = c('black'),
#     xlim = c(0,84), 
#     survFileName = 'relapses_all_LoT.pdf',
#     survExcelName = 'relapses_all_LoT.xlsx',
#     strataCol = 'lineTreatment',
#     demographicsTable = demographicsTable)
#   
#   #all - line of treatment
#   getRelapsesPerDrug(
#     relapseTable,
#     drugTable,
#     initialDate = '2010-01-01', 
#     drugsToExclude = c(),
#     curvColours = c('black'),
#     xlim = c(0,48), 
#     survFileName = 'relapses_all_smoking.pdf',
#     survExcelName = 'relapses_all_smoking.xlsx',
#     strataCol = 'smokingEver',
#     demographicsTable = demographicsTable)
#   
#   
#   
# }
# 
# 
# 
# 
# 
# 
# 
# 
# # smDriver = JDBC("com.vertica.jdbc.Driver", classPath = "C:\\Program Files/Vertica Systems/JDBC/vertica-jdbc-7.2.2-0.jar")
# # sm <- dbConnect(smDriver, "jdbc:vertica://vertica-als-lb.explorys.net:5433/db1", "Martha.Imprialou", "Wm7c2B4A")
# # 
# # #q1 <- dbGetQuery(sm, 'SELECT explorys_patient_id FROM supermart_200.v_drug WHERE prescription_date > Convert(datetime, "2016-01-01")')
# # 
# # #msHospitalizations = dbConnect(sm, 'SELECT diagnosis_date')
# # 
# # hosp <- dbGetQuery(sm, "SELECT explorys_patient_id, encounter_join_id, std_encounter_type, appt_made_date, encounter_date FROM supermart_200.v_encounter WHERE std_encounter_type IN ('HOSPITAL_ENCOUNTER', 'HOSPITAL_EMERGENCY_ROOM_VISIT', 'HOSPITAL_INPATIENT', 'URGENT')")
# # 
# # ms_diagnoses <- dbGetQuery(sm, "SELECT 
# #                            explorys_patient_id, icd_code, encounter_join_id, diagnosis_date, is_principal, is_admitting, is_reason_for_visit 
# #                            FROM supermart_200.v_diagnosis WHERE icd_code LIKE '%340%' OR icd_code LIKE '%G35%'")
# # 
# # 
# # 
# # 
# # #### get relapse rates
# # #### 1. from v_diagnosis get all patients 
# # 
# # diag <- dbGetQuery(sm, 'SELECT * FROM supermart_200.v_diagnosis LIMIT 5')
# # 
# # #### 2. from v_drug - get all patients that have been prescribed corticosteroids
# # #### for an MS diagnosis
# # 
# # 
# # 
# # 
# # 
# 
# #### temp: move out of variables.R
# #### check what diagnoses were there for patients with hospitalizations
# #### is it safe to assume that they were due to MS?
# hospDiagnoses <- function(){
#   
#   hospitalizationsAllDiag <- dbGetQuery(
#     env$sm, "SELECT d.encounter_join_id, explorys_patient_id, icd_code, std_encounter_type, encounter_date FROM
#     (SELECT 
#     encounter_join_id, explorys_patient_id, icd_code 
#     FROM supermart_200.v_diagnosis) d
#     INNER JOIN (SELECT 
#     encounter_join_id, std_encounter_type, encounter_date 
#     FROM supermart_200.v_encounter 
#     WHERE std_encounter_type IN ('HOSPITAL_ENCOUNTER', 'HOSPITAL_EMERGENCY_ROOM_VISIT', 'HOSPITAL_INPATIENT', 'URGENT')) e
#     ON d.encounter_join_id = e.encounter_join_id"
#   )
#   
#   hospitalizationsAllDiag <- hospitalizationsAllDiag %>% 
#     group_by(explorys_patient_id, encounter_join_id) %>%
#     mutate(
#       hasMS = any(sapply(c('340', 'G35'), grepl, icd_code))
#     ) %>% filter(hasMS)
#     
#   #diagnoses by hospitalization
#   nDiagByHosp = hospitalizationsAllDiag %>%
#     group_by(encounter_join_id) %>%
#     summarise(
#       nDiagnoses = length(unique(icd_code))
#     )
#   
#   nDiagByHospPlot <- ggplot(nDiagByHosp, aes(x = nDiagnoses)) + 
#     geom_histogram(binwidth = 1, col = 'navyblue') + 
#     xlab('number of diagnoses') + 
#     ylab('number of encounters')
#   
#   ggsave(filename = paste0(env$outputDir, 'nDiagnosesByHosp.pdf'), nDiagByHospPlot)
#   
#   diagCount <- hospitalizationsAllDiag %>%
#     group_by(icd_code) %>%
#     summarise(
#       count = length(unique(encounter_join_id))
#     ) %>% arrange(desc(count))
#   
#   diagCount$icd_code = factor(diagCount$icd_code, levels = diagCount$icd_code)
#   
#   ###### count co-diagnoses of relapses in early Lemtrada switchers
#   load(paste0(env$saveDir, 'drugVsRelapse.Rdata'))
#   
#   lemtradaPatients <- drugVsRelapse %>%
#     filter(molecule_name == 'alemtuzumab') %>%
#     group_by(explorys_patient_id) %>%
#     summarise(
#       timeOnLemtrada = elapsed_months(max(prescriptionMonth), min(prescriptionMonth)) + 1,
#       firstPrescDate = unique(firstPrescDate),
#       lastPrescDate  = as.Date(paste0(format(as.Date(max(prescriptionMonth)), '%Y-%m'), '-28'))
#     )
#   
#   lemtradaHosp <- lemtradaPatients %>%
#     left_join(hospitalizationsAllDiag) %>%
#     filter(is.na(encounter_date) | (encounter_date > firstPrescDate & encounter_date <= lastPrescDate) ) 
#   
#   lemtradaDiagCount <- lemtradaHosp %>%
#     group_by(icd_code) %>%
#     summarise(
#       count = length(unique(encounter_join_id))
#     ) %>% arrange(desc(count))
#   
#   lemtradaDiagCount$icd_code = factor(lemtradaDiagCount$icd_code, levels = lemtradaDiagCount$icd_code)
#   
#   
#   
#   otherDiagnosesHosp <- ggplot(diagCount[3:23,], aes(x = icd_code, y = count)) + 
#     geom_bar(stat = 'identity')
#   
#   ggsave(filename = paste0(env$outputDir, 'top20CoDiagnosesHops.pdf'), otherDiagnosesHosp, height = 5, width = 12)
#   
#   lemtradaOtherDiagnosesHosp <- ggplot(lemtradaDiagCount[3:23,], aes(x = icd_code, y = count)) + 
#     geom_bar(stat = 'identity')
#   
#   ggsave(filename = paste0(env$outputDir, 'top20CoDiagnosesHospLemtrada.pdf'), lemtradaOtherDiagnosesHosp, height = 5, width = 12)
#   
#   
#   
#   
# }
# 
# 
