PhysiciansDetails<-function(allDrugsInfo,naivePatients){
  physicianPatient<-as.data.table(dbGetQuery(env$sm,"select explorys_patient_id, 
                                             provider_id_hash, 
                                             encounter_join_id,
                                             std_provider_type,
                                             specialty_name_1,
                                             specialty_name_2
                                             from supermart_200_S_02_10_2017.v_encounter_provider 
                                             where std_provider_type like 'PHYSICIAN'"))
  physicianPatientNaive<-unique(physicianPatient[explorys_patient_id%in%naivePatients$explorys_patient_id])
  physiciansMSDrugNaive <-as.data.table(merge.data.frame(physicianPatientNaive,allDrugsInfo,by='encounter_join_id'))
  
  physicianSpecialty<-unique(physiciansMSDrugNaive[,.(provider_id_hash,specialty_name_1)])
  table(physicianSpecialty$specialty_name_1)
  
  }